#!/usr/bin/env bash

git clone https://gitlab.com/Xangelix/gitpod-signed-workspace
rsync -a ./gitpod-signed-workspace/ . --exclude={'README.md','.git/'}
rm -rf ./gitpod-signed-workspace/
sudo chmod +x ./.gitpod.update.sh
